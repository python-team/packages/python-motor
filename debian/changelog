python-motor (3.6.0-1) unstable; urgency=low

  * New upstream release

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Wed, 09 Oct 2024 15:30:59 +0200

python-motor (3.5.1-1) unstable; urgency=low

  * New upstream release
  * Build-depend on python3-hatch-requirements-txt
  * Build-depend on pybuild-plugin-pyproject

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Tue, 27 Aug 2024 09:07:44 +0200

python-motor (3.4.0-4) unstable; urgency=low

  * Revert the previous changes

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Thu, 27 Jun 2024 10:27:16 +0200

python-motor (3.4.0-3) unstable; urgency=low

  * Set myself as maintainer
  * Point to new Vcs in Debian, since I don't have write access to the one in the python team

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Wed, 26 Jun 2024 07:21:05 +0200

python-motor (3.4.0-2) unstable; urgency=low

  * Remove pkgtest, due to mongodb no longer being in debian (Closes: #989408)

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Wed, 19 Jun 2024 10:37:22 +0200

python-motor (3.4.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Remove myself from Uploaders.

  [ Salvo 'LtWorf' Tomaselli ]
  * New upstream version (Closes: #1073557)
  * Add myself to uploaders
  * Drop version in dependencies
  * Remove upstreamed patches

 -- Salvo 'LtWorf' Tomaselli <ltworf@debian.org>  Mon, 17 Jun 2024 15:13:41 +0200

python-motor (2.3.0-3) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.2, no changes needed.

  [ Étienne Mollier ]
  * python-3.11.patch: add patch from upstream.
    This fixes an import error of motor.motor_asyncio.
    Thanks to Andrey Rakhmatullin for the hint!  (Closes: #1031763)

 -- Étienne Mollier <emollier@debian.org>  Fri, 03 Mar 2023 14:29:00 +0100

python-motor (2.3.0-2) unstable; urgency=medium

  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.5.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 26 May 2022 19:39:58 +0100

python-motor (2.3.0-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.
  * Bump required version of python3-pymongo to 3.11.
  * Mark Rules-Requires-Root: no.
  * Bump debhelper compat level to 13.
  * Bump standards version to 4.5.0.
  * d/p/new-crt.patch: Add metadata.

 -- Ondřej Nový <onovy@debian.org>  Tue, 27 Oct 2020 10:32:59 +0100

python-motor (2.1.0-2) unstable; urgency=medium

  * Restore new-crt.patch.

 -- Ondřej Nový <onovy@debian.org>  Sun, 12 Jan 2020 15:56:25 +0100

python-motor (2.1.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Balint Reczey ]
  * Add minimal Salsa CI config
  * tests: Expect asyncio.exceptions.TimeoutError where it is present.
    This fixes test failure with Python 3.8
  * New upstream version 2.1.0
  * Drop patches integrated upstream
  * Depend on python3-pymongo (>= 3.10)

 -- Ondřej Nový <onovy@debian.org>  Sun, 12 Jan 2020 14:43:45 +0100

python-motor (2.0.0-3) unstable; urgency=medium

  * Rename d/tests/control.autodep8 to d/tests/control.
  * Bump standards version to 4.3.0 (no changes)
  * Bump debhelper compat level to 12
  * Drop Python 2 support

 -- Ondřej Nový <onovy@debian.org>  Mon, 08 Jul 2019 13:52:00 +0200

python-motor (2.0.0-2) unstable; urgency=medium

  * Regenerate test certificates, make it compatible with OpenSSL 1.1.1
  * d/tests: Require newer MongoDB server
  * Bump standards version to 4.2.1 (no changes)

 -- Ondřej Nový <onovy@debian.org>  Sat, 29 Sep 2018 22:24:43 +0200

python-motor (2.0.0-1) unstable; urgency=medium

  * New upstream release
  * Convert git repository from git-dpm to gbp layout
  * d/p/0001-MOTOR-248-Don-t-use-async-as-variable-name.patch:
    Drop, applied upstream
  * d/tests: Bump required version of python-mockupdb
  * Bump standards version to 4.2.0 (no changes)

 -- Ondřej Nový <onovy@debian.org>  Mon, 20 Aug 2018 13:22:19 +0200

python-motor (1.2.3-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add upstream fix for compatibility with Python 3.7.
    (Closes: #903527)

 -- Adrian Bunk <bunk@debian.org>  Fri, 10 Aug 2018 23:31:48 +0300

python-motor (1.2.3-1) unstable; urgency=medium

  * New upstream release
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Bump required version of pymongo to 3.6
  * Add upstream metadata
  * Standards-Version is 4.1.4 now (no changes needed)

 -- Ondřej Nový <onovy@debian.org>  Tue, 29 May 2018 13:30:50 +0200

python-motor (1.2.1-1) unstable; urgency=medium

  * New upstream release
  * d/copyright: Bump upstream copyright year

 -- Ondřej Nový <onovy@debian.org>  Mon, 22 Jan 2018 09:41:36 +0100

python-motor (1.2.0-1) unstable; urgency=medium

  * New upstream release
  * d/rules: Don't remove aiohttp and asyncio in Python 2 (fixed upstream)
  * d/copyright: Bump my copyright year
  * Bump debhelper compat level to 11
  * Standards-Version is 4.1.3 now (no changes needed)
  * debian/tests: Run tests on all Python versions

 -- Ondřej Nový <onovy@debian.org>  Tue, 16 Jan 2018 15:32:57 +0100

python-motor (1.1-2) unstable; urgency=medium

  * Uploading to unstable

 -- Ondřej Nový <onovy@debian.org>  Tue, 20 Jun 2017 12:37:33 +0200

python-motor (1.1-1) experimental; urgency=medium

  * New upstream release
  * Bump required version of PyMongo to 3.4
  * d/copyright: Bump copyright years
  * d/s/options: Ignore .egg-info
  * Remove not needed B-D which are useful only for tests
  * Add python{,3}-gridfs and python-concurrent.futures to Depends
  * Suggests python{,3}-tornado and python3-aiohttp
  * d/tests: Run upstream unit tests

 -- Ondřej Nový <onovy@debian.org>  Sat, 08 Apr 2017 15:40:45 +0200

python-motor (1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #816969)

 -- Ondřej Nový <onovy@debian.org>  Tue, 15 Nov 2016 18:10:05 +0100
